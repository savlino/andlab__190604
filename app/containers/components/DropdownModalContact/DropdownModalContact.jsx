import React, {Component} from "react";

export default class DropdownModalContact extends Component {
  render() {
    const {items, onSelect, isOpen} = this.props;
    return (
      <div className="new-message-dropdown">
        {isOpen && (
          <div>
            {items.map(item => (
              <div
                className="dropdown-elem"
                onClick={() => onSelect(item.text)}
              >
                {item.text}
                <div className="adress-line">
                  {item.mail}
                </div>
              </div>
            ))}
          </div>
        )}
      </div>
  )}
}