import React, {Component} from "react";
import moment from 'moment';
import DropdownInput from '../DropdownInput/DropdownInput';

export default class LetterSearch extends Component {
  state = {
    dateRangeOpen: false,
  }
  
  openDateRange = (e) => {
    this.setState({
      dateRangeOpen: true,
    });
    e.target.addEventListener("click", this.closeDateRange);
  }

  closeDateRange = (e) => {
    this.setState({
      dateRangeOpen: false,
    });
    e.target.removeEventListener("click", this.closeDateRange);
  }

  render() {
    const tab = this.props.tab;
    const shortcuts = [
      {text: "за сегодня", value: moment().format('DD.MM.YYYY')},
      {text: "за вчера", value: moment().subtract(1, 'days').format('DD.MM.YYYY')},
      {text: "за неделю", value: moment().subtract(7, 'days').format('DD.MM.YYYY')},
      {text: "за месяц", value: moment().subtract(1, 'months').format('DD.MM.YYYY')},
      {text: "за год", value: moment().subtract(1, 'years').format('DD.MM.YYYY')}
    ]

    return(
      <form className="search-container">
        <label
          onClick={this.openDateRange}
        >
          <div className="with-arrow">
            <input
              className="search-date-input"
              type="text"
              value={this.props.range}
              onSelect={this.closeDateRange}
              readOnly
            />
          </div>
          <DropdownInput
            onSelect={this.props.handleChange}
            isOpen={this.state.dateRangeOpen}
            items={shortcuts}
          />
        </label>
        {(tab === "All") && <input
          className="search-form text-form"
          placeholder="Поиск по контрагенту, счету, значению..."
        />}
        {(tab === "in") && <input
          className="search-form text-form"
          placeholder="Поиск по входящим"
        />}
        {(tab === "out") && <input
          className="search-form text-form"
          placeholder="Поиск по отправленным"
        />}
        {(tab === "draft") && <input
          className="search-form text-form"
          placeholder="Поиск по черновикам"
        />}
      </form>
    )
  }
}