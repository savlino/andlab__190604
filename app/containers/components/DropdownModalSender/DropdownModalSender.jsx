import React, {Component} from "react";

export default class DropdownModalSender extends Component {
  render() {
    const {items, onSelect, isOpen} = this.props;
    return (
      <div>
        {isOpen && (
          <div className="new-message-dropdown">
            {items.map(item => (
              <div
                className="dropdown-elem"
                onClick={() => {onSelect(item.text)}}
              >
                {item.text}
              </div>
            ))}
          </div>
        )}
      </div>
  )}
}