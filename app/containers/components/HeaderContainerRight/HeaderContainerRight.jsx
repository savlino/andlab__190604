import React, {Component} from "react";
import calend from '../../../images/calend.png';
import bell from '../../../images/bell.png';

export default class HeaderContainerRight extends Component {
  render() {
    return(
      <div className="header-container-right">
        <a className="header-button" href="#">
          <img className="header-button-image" src={calend} alt=""/>
        </a>
        <a className="header-button" href="#">
          <img className="header-button-image" src={bell} alt=""/>
        </a>
        {(this.props.letterCounter > 0) && 
          <div className="unreads-bubble">
            {this.props.letterCounter}
          </div>
        }
        <div className="select-container">
          <select className="account-selector">
            <option>ООО "CтальПромДобыча"</option>
          </select>
        </div>
      </div>
    )
  }
}