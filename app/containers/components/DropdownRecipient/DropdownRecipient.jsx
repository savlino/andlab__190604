import React, {Component} from "react";

export default class DropdownRecipient extends Component {
  state = {
    open: false,
  }

  open = () => {
    this.setState({
      open: true,
    });
    document.addEventListener("click", this.close);
  }

  close = () => {
    this.setState({
      open: false,
    });
    document.removeEventListener("click", this.close);
  }

  extraRecip = this.props.recipients.slice(1).map(
    (item, index) => 
    <div className="recip-dropdown-elem">
      <div>{item.name}</div>
      <div className="recip-email">{item.email}</div>
    </div>
  );

  render() {
    return (
      <React.Fragment>
        {(this.props.recipients.length === 2) &&
          <button
            className="extra-recipients-btn"
            onClick={this.open}
          >
            ещё 1 получатель
          </button>
        }
        {(this.props.recipients.length > 2) &&
          <button
            className="extra-recipients-btn"
            onClick={this.open}
          >
            ещё {this.props.recipients.length - 1} получателя
          </button>
        }
        {this.state.open &&
          <div className="recip-dropdown">
            {this.extraRecip}
          </div>
        }
      </React.Fragment>
    );
  }
}