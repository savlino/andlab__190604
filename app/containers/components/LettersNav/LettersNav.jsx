import React, {Component} from "react";
import NavElem from '../NavElem/NavElem';

export default class LettersNav extends Component {
  render() {
    const tabs = this.props.menus.map(
      (elem, index) => 
      <NavElem
        key={index}
        elem={elem}
        isCurrent={this.props.tab}
        handleClick={this.props.handleClick}
      />
    )
    
    return(
      <menu className="letters-nav-container">
        {tabs}
      </menu>
    )
  }
}