import React, {Component} from "react";
import moment from 'moment';
import LettersHead from '../LettersHead/LettersHead';
import NewMessage from '../NewMessage/NewMessage';
import LettersNav from '../LettersNav/LettersNav';
import LetterSearch from '../LetterSearch/LetterSearch';
import LetterBlock from '../LetterBlock/LetterBlock';

export default class Letters extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalNewIsOpen: false,
      currentTab: 'All',
      currentRange: '__.__.____ - ' + moment().format('DD.MM.YYYY'),
      menus: [
        {label: "All", text: "Все"},
        {label: "in", text: "Входящие"},
        {label: "out", text: "Отправленные"},
        {label: "draft", text: "Черновики"}
      ],
      letters: this.props.letters,
      sender: '',
    };

    this.handleTabChange = this.handleTabChange.bind(this);
    this.handleRangeChange = this.handleRangeChange.bind(this);
    this.handleSenderChange = this.handleSenderChange.bind(this);
  }

  closeModal = () => {
    this.setState({modalNewIsOpen: false});
  }

  openNewMessage = () => {
    this.setState({modalNewIsOpen: true});
  }

  openNewMessageWithSender = (sender) => {
    this.setState({modalNewIsOpen: true});
    this.setState({sender});
  }

  handleSenderChange(sender) {
    this.setState({sender})
  }

  handleTabChange(currentTab) {
    return e => {
      e.preventDefault();
      this.setState({
        currentTab,
      });
    }
  }

  handleRangeChange(currentRange) {
    this.setState({
      currentRange: `${currentRange} - ${moment().format('DD.MM.YYYY')}`,
    })
  }

  render() {
    return (
      <div>
        <LettersHead onOpenForm={this.openNewMessage}/>
        <LettersNav 
          tab={this.state.currentTab}
          handleClick={this.handleTabChange}
          menus={this.state.menus}
        />
        <NewMessage
          isOpen={this.state.modalNewIsOpen}
          onClose={this.closeModal}
          sender={this.state.sender}
          onSenderSelect = {this.handleSenderChange}
        />
        <LetterBlock
          letters={this.state.letters}
          tab={this.state.currentTab}
          range={this.state.currentRange}
          handleRespond={this.openNewMessageWithSender}
        />
        <LetterSearch
          range={this.state.currentRange}
          handleChange={this.handleRangeChange}
          tab={this.state.currentTab}
        />
      </div>
    )
  }
}