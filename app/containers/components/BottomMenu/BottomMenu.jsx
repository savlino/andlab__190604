import React, {Component} from "react";

export default function BottomMenu() {
  return(
    <menu className="bottom-nav-container">
      <li>Благотворительность</li>
      <li>Настройки</li>
      <li>Тарифы и договор</li>
      <li>Контакты и офисы</li>
      <li>Выйти</li>
    </menu>
  )
}