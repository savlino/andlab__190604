import React, {Component} from "react";

export default class DropdownInput extends Component {
  render() {
    const {items, onSelect, isOpen} = this.props;
    return (
      <div className="dropdown">
        {isOpen && (
          <div>
            {items.map(item => (
              <div
                className="dropdown-elem"
                onClick={() => onSelect(item.value)}
              >
                {item.text}
              </div>
            ))}
          </div>
        )}
      </div>
    );
  }
}