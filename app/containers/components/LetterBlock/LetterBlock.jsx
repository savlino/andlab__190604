import React, {Component} from "react";
import LetterItem from '../LetterItem/LetterItem';

export default class LetterBlock extends Component {
  state = {
    allIsChecked: false,
  }
  
  handleCheckAll = (e) => {
    const {allIsChecked} = this.state;
    this.setState({allIsChecked: !allIsChecked});
  }

  render() {
    const lettersArr = this.props.letters.map((letter, index) =>
      (this.props.tab === letter.type || this.props.tab === "All") &&
        <LetterItem
          letter={letter}
          key={index}
          name={"letter-checkbox-" + index}
          tab={this.props.tab}
          checkFromParent={this.state.allIsChecked}
          handleRespond={this.props.handleRespond}
        />
    )
    return(
      <div className="letters-container">
        <form className="all-check">
          <input
            type="checkbox"
            name="all-check"
            onClick={this.handleCheckAll}
          /> 
          Выбрать все
        </form>
        <div className="letter-items-container">
          {lettersArr}
        </div>
      </div>
    )
  }
}