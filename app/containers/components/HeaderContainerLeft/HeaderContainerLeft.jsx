import React, {Component} from "react";
import vtb_logo from '../../../images/vtb_logo.png';

export default function HeaderContainerLeft() {
  return(
    <div className="header-container-left">
      <a href="#" className="header_logo">
        <img src={vtb_logo} />
      </a>
      <a className="button-back">назад</a>
    </div>
  )
}