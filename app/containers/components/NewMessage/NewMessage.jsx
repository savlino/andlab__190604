import React, {Component} from "react";
import Modal from 'react-modal';
import DropdownModalSender from '../DropdownModalSender/DropdownModalSender';
import DropdownModalContact from '../DropdownModalContact/DropdownModalContact';
import def_ava from '../../../images/def_ava.png';
import clip_blue from '../../../images/clip_blue.png';

export default class NewMessage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sender: '',
      contact: '',
      title: '',
      letterBody: '',
      contactsOpen: false,
      senderOpen: false,
      docAttached: true,
      pdfAttached: true,
      isFilled: false,
    };
    this.detachFile = this.detachFile.bind(this);
    this.handleContactChange = this.handleContactChange.bind(this);
    this.showInputError = this.showInputError.bind(this);
  }

  handleContactChange(contact) {
    this.setState({contact});
  }

  openContacts = (e) => {
    this.setState({
      contactsOpen: true,
    });
    e.target.addEventListener("click", this.closeContacts);
  }

  closeContacts = (e) => {
    this.setState({
      contactsOpen: false,
    });
    e.target.removeEventListener("click", this.closeContacts);
  }

  openSender = (e) => {
    this.setState({
      senderOpen: true,
    });
    e.target.addEventListener("click", this.closeSender);
  }

  closeSender = (e) => {
    this.setState({
      senderOpen: false,
    });
    e.target.removeEventListener("click", this.closeSender);
  }

  handleLetter = (e) => {
    const letterBody = e.target.value;
    this.setState({letterBody})
  }

  handleTitle = (e) => {
    const title = e.target.value;
    this.setState({title})    
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const inputs = [this.refs.title, this.refs.contactDD, this.refs.senderDD, this.refs.letterBody];
    inputs.forEach(input => this.showInputError(input.name || input.props.name));
    if(this.props.sender && this.state.contact && this.state.title && !/\W/i.test(this.state.letterBody)) {
      this.props.onClose();
      alert('Письмо отправлено');
    }
  }
  
  showInputError = (refName) => {
    const input = this.refs[refName];
    const error = this.refs[refName + 'Error'];
    if (refName === "title") {
      const validity = this.refs[refName].validity;
      if (validity.valueMissing) {
        input.classList.add('error-form');
        error.textContent = 'Укажите тему';
        return;
      }
      input.classList.remove('error-form');
      error.textContent = '';
    } 
    else if (refName === "contact") {
      const validity = this.refs[refName].validity;
      if (validity.valueMissing) {
        input.classList.add('error-form');
        error.textContent = 'Укажите получателя';
        return;
      }
      input.classList.remove('error-form');
      error.textContent = '';
    } else if (refName === "sender") {
      const validity = this.refs[refName].validity;
      if (validity.valueMissing) {
        input.classList.add('error-form');
        error.textContent = 'Укажите организацию';
        return;
      }
      input.classList.remove('error-form');
      error.textContent = '';
    } else if (refName === "letterBody") {
      if (/\W/i.test(input.value)) {
        input.classList.add('error-textarea');
        error.textContent = 'В тексте сообщения недопустимые символы';
        return;
      }
      input.classList.remove('error-textarea');
      error.textContent = '';
    }
  }

  detachFile(e) {
    name = e.target.id + 'Attached';
    this.setState({[name]: false});
  }

  senderList = [
    {text: 'ООО "CтальПромДобыча"'},
  ];
  contactList = [
    {text: "Нижегородский филиал ВТБ", mail: "bco_otzyvgo@vtb24.ru"},
    {text: "Рязанский филиал ВТБ", mail: "bo@vtb24.ru"}
  ]

  render() {
    return(
      <Modal
        isOpen={this.props.isOpen}
        onRequestClose={this.props.onClose}
        contentLabel="write new message"
        className="modal new-message-modal"
      >
        <button className="modal-close" onClick={this.props.onClose}>⨯</button>
        <h2>Новое письмо</h2>
        <form className="new-message-form" noValidate>
          <label>
            Oт имени организации
            <div className="sender-select">
              <input
                name="sender"
                ref="sender"
                type="text"
                value={this.props.sender}
                onClick={this.openSender}
                onSelect={this.closeSender}
                className="new-message-field"
                required
              />
            </div>
            <DropdownModalSender
              name="sender"
              ref="senderDD"
              onSelect={this.props.onSenderSelect}
              isOpen={this.state.senderOpen}
              items={this.senderList}
            />
            <div className="error" ref="senderError" />
          </label>
          <label>
            Получатель
            <div className="contact-select">
              <input
                name="contact"
                ref="contact"
                type="text"
                value={this.state.contact}
                onClick={this.openContacts}
                onSelect={this.closeContacts}
                className="new-message-field"
                required
              />
              <img src={def_ava} className="def_ava" /> 
            </div> 
            <DropdownModalContact
              name="contact"
              ref="contactDD"
              onSelect={this.handleContactChange}
              isOpen={this.state.contactsOpen}
              items={this.contactList}
            />
            <div className="error" ref="contactError" />
          </label>
          <label>
            Тема
            <input
              name="title"
              ref="title"
              type="text"
              className="new-message-field"
              value={this.state.title}
              onInput={this.handleTitle}
              required
            />
            <div className="error" ref="titleError" />
          </label>
          <label>
            <textarea
              ref="letterBody"
              rows="7"
              className="new-message-text"
              name="letterBody"
              placeholder="Введите текст письма"
              onChange={this.handleLetter}
              value={this.state.letterBody}
            />
            <div className="error" ref="letterBodyError" />
          </label>
          {this.state.docAttached && <div className="attached-file">
            <div className="file-type">DOC</div>
            <a href="#" className="file-name">
              Договор на продажу.docx
            </a>
            <div>24 КБ</div>
            <div className="file-detach" onClick={this.detachFile} id="doc" >⨯</div>
          </div>}
          {this.state.pdfAttached && <div className="attached-file">
            <div className="file-type">PDF</div>
            <a href="#" className="file-name">
              Текущий состав участников встречи.pdf
            </a>
            <div>42 КБ</div>
            <div className="file-detach" onClick={this.detachFile} id="pdf" >⨯</div>
          </div>}
          <div className="file-attach">
            <img src={clip_blue} />
            <button className="file-attach-name">Прикрепить файл</button>
          </div>
          <button className="new-msg-btn modal-btn-prime" onClick={this.handleSubmit}>
            Отправить
          </button>
          <button className="new-msg-btn modal-btn-second">
            Сохранить в черновики
          </button>
        </form>
      </Modal>
    )
  }
}