import React, {Component} from "react";
import Modal from 'react-modal';
import moment from 'moment';
import env from '../../../images/env.png';
import clip from '../../../images/clip.png';
import ShowMessage from '../ShowMessage/ShowMessage';

export default class LetterItem extends Component {
  static getDerivedStateFromProps(nextProps, prevState) {
    if((nextProps.checkFromParent !== prevState.checked) && nextProps.checkFromParent) {
      return {modalShow: prevState.modalShow, checked: nextProps.checkFromParent}
    } else {
      return {modalShow: prevState.modalShow, checked: prevState.checked}
    }
    return null;
  }

  state = {
    modalShow: false,
  }

  closeModal = () => {
    this.setState({modalShow: false});
  }

  openModal = () => {
    this.setState({modalShow: true});
  }

  handleCheckbox = () => {
    const {checked} = this.state;
    console.log(checked);
    this.setState({checked: !checked});
  }

  render() {
    const {type, specialType, status, withFile, historyInfo, date, letterTitle, letterSender, checkFromParent} = this.props.letter;
    return(
      <React.Fragment>
        <div className="letter-item">
          <div className="letter-checkbox">
            <input
              name={this.props.name}
              type="checkbox"
               className="letter-checkbox"
              checked={this.state.checked}
              onChange={this.handleCheckbox}
            />
          </div>
          <div className="letter-left" onClick={this.openModal}>
            <div className="letter-date">
              {moment(date, "ddd MMM DD YYYY HH:mm:ss").format("DD MMMM, HH:mm")}
              <br/>
              {moment().to(date)}
            </div>
          </div>
          <div className="letter-right" onClick={this.openModal}>
            {(status === "new") &&
              <div className="new-item-sign"></div>
            }
            {specialType &&
              <div className="imp-item-sign">ВАЖНО</div>
            }
            <div className="item-title">
              {letterSender.name}
            </div>
            <div className="item-text">
              {letterTitle}
            </div>
          </div>
          <div className="letter-extras" onClick={this.openModal}>
            <div className="letter-history">
              {(historyInfo.countHistory > 0) && <p>{historyInfo.countHistory}</p>}
              {historyInfo.containHis && <img src={env}/>}
            </div>
            {withFile &&
              <img src={clip}/>
            }
          </div>
        </div>
        <ShowMessage
          isOpen={this.state.modalShow}
          onClose={this.closeModal}
          letter={this.props.letter}
          handleRespond={this.props.handleRespond}
        />
      </React.Fragment>
    )
  }
}