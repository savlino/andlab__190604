import React, {Component} from "react";
import HeaderContainerLeft from "../HeaderContainerLeft/HeaderContainerLeft";
import HeaderContainerRight from "../HeaderContainerRight/HeaderContainerRight";

export default class Header extends Component {
  render() {
    return (
      <div className="header-body clearfix">
        <HeaderContainerLeft />
        <HeaderContainerRight letterCounter={this.props.letters.length} />
      </div>
    )
  }
}