import React, {Component} from "react";
import profile_icon from '../../../images/profile-icon.png';

export default class AccountInfo extends Component {
  render() {
    return(
      <div className="accountinfo-container">
        <div className="avatar-container">
          <img src={profile_icon} />
        </div>
        <div className="account-name">{this.props.name}</div>
      </div>
    )
  }
}