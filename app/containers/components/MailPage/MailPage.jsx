import React, {Component} from "react";
import Header from '../Header/Header';
import SideBar from '../SideBar/SideBar';
import Letters from '../Letters/Letters';
import * as data from '../LettersData/LettersData';

export default function MailPage () {
	return (
		<React.Fragment>
			<Header letters={data.letters} />
			<SideBar letters={data.letters} />
			<Letters letters={data.letters} />
		</React.Fragment>
	)
}