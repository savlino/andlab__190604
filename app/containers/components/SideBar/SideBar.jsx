import React, {Component} from "react";
import AccountInfo from '../AccountInfo/AccountInfo';
import MainMenu from '../MainMenu/MainMenu';
import BottomMenu from '../BottomMenu/BottomMenu';
import chat_logo from '../../../images/chat-logo.png';

export default class SideBar extends Component {
  state = {
    name: 'Иванов Иван Иванович',
    img: '',
  }
  
  render() {
    return(
      <div className="sidebar-container">
        <AccountInfo name={this.state.name} img={this.state.img} />
        <div className="menu-container">
          <MainMenu unreads={this.props.letters.length}/>
          <BottomMenu />
        </div>
        <a href="#" className="chat-link">
          <img src={chat_logo} />
        </a>
      </div>
    )
  }
}