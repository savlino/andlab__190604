import React, {Component} from "react";
import Modal from 'react-modal';
import moment from 'moment';
import print from '../../../images/print.png';
import DropdownRecipient from '../DropdownRecipient/DropdownRecipient';

export default class ShowMessage extends Component {
  constructor(props) {
    super(props);
  }

  respond = (e) => {
    e.preventDefault();
    this.props.onClose();
    this.props.handleRespond('ООО "CтальПромДобыча"');
  }

  render() {
    const {withFile, letterTitle, date, letterMessage, letterSender, letterRecipients} = this.props.letter;
    return(
      <Modal
        isOpen={this.props.isOpen}
        onRequestClose={this.props.onClose}
        className="modal show-message-modal"
      >
        <button className="modal-close" onClick={this.props.onClose}>⨯</button>
        <h3>{letterTitle}</h3>
        <div className="letter-credits">
          <div className="letter-credits-title">
            <div>от</div>
            <div>кому</div>
          </div>
          <div className="letter-credits-moment">
            {moment(date, "ddd MMM DD YYYY HH:mm:ss").format("DD MMMM, HH:mm")} ({moment().to(date)})
          </div>
          <div className="letter-credits-value">
            <div>              
              {letterSender.name}
              <div className="letter-credits-mail">{'<' + letterSender.email + '>'}</div>
            </div>
          </div>
          <div className="letter-credits-value">
            {letterRecipients[0].name}
            <div>
              <DropdownRecipient recipients={letterRecipients} />
            </div>
          </div>
        </div>
        <div className="letter-text">
          {letterMessage}
        </div>
        <div className="letter-attachments">
          <div className="attached-file">
            <div className="file-type">DOC</div>
            <a href="#" className="file-name">Договор на продажу.docx</a>
            <div className="file-size">24 КБ</div>
          </div>
        </div>
        <button className="new-msg-btn modal-btn-prime" onClick={this.respond} >Ответить</button>
        <button className="new-msg-btn modal-btn-second">Переслать</button>
        <button className="modal-btn-print"> <img src={print} /> </button>
      </Modal>
    )
  }
}