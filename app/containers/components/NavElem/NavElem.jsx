import React, {Component} from "react";

export default class NavElem extends Component {
  render() {
    if (this.props.isCurrent == this.props.elem.label) {
      return(
        <button
          className={"letters-nav-btn letters-nav-current"}
          onClick={ this.props.handleClick(this.props.elem.label)}
        >
          {this.props.elem.text}
        </button>
    )}
    return(
      <button
        className={"letters-nav-btn"}
        onClick={this.props.handleClick(this.props.elem.label)}
      >
        {this.props.elem.text}
      </button>
    )
  }
}