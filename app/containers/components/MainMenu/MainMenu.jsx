import React, {Component} from "react";
import menu_button from '../../../images/menu-button.png';

export default class MainMenu extends Component {
  render() {
    return(
      <menu className="main-menu">
        <li className="selected-item">Главная</li>
        <li>Счета</li>
        <li>Выписка</li>
        <li>Письма<div className="menu-bubble">{this.props.unreads}</div></li>
        <li className="submenu">Продукты</li>
        <li className="submenu">Сервисы</li>
        <a href="#" className="menu-button"><img src={menu_button} /></a>
      </menu>
    )
  }
}