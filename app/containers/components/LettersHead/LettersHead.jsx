import React, {Component} from "react";

export default class LettersHead extends Component {
  constructor(props) {
    super(props);
  }

  newMessage = () => {
    this.props.onOpenForm();
  }

  render() {
    return(
      <div className="head-container">
        <h2 className="head-name">Письма</h2>
        <button className="head-button blue-one" onClick={this.newMessage}>Написать письмо</button>
        <button className="head-button white-one">Заказать справку</button>
      </div>
    )
  }
}