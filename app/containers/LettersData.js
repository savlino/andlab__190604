export const letters = [
  {
    type: "draft",  // Тип письма входящее(in), исходящее(out), черновик(draft)
    specialType: true, // Пометка "важное"
    status: "new", // Статус письма прочитанное(readed), непрочитанное(new)
    withFile: true, // Наличие прикрепленных файлов
    historyInfo: {
      containHis: true, // Есть ли прикрепленная история писем
      countHistory: 6, // Счетчик писем в истории
    },
    letterTitle: 'Договор на поставку 5 метров лески', // Тема письма
    date: "Tue Jun 04 2019 12:40:48 GMT+0300 (Москва, стандартное время)", //Время отправления письма
    letterMessage: "Высылаю Вам на ознакомление исправленное доп.соглашение к текущему договору.", // Содержимое письма
    letterSender: {
      name: "ООО 'Северсталь'",  // Отправитель письма
      email: "sever@severstal.com" // ящик отправителя
      
    },
    letterRecipients: [              // Получатели письма (может быть от одного до пяти)   
    {
      name: "ООО 'МТС'",  // Получатель письма
      email: "mts@mts.com" // ящик Получателя
      
    },
    {
      name: "ЗАО 'КОСМОС'",  // Получатель письма
      email: "info@cosmos.ru" // ящик Получателя
      
    }]
  },

  {
    type: "in",
    specialType: false,
    status: "new",
    withFile: false,
    historyInfo: {
      containHis: false,
      countHistory: 0,
    },
    letterTitle: 'Договор на поставку 100 тонн леса',
    date: "Tue Jun 04 2019 22:12:42 GMT+0300 (Москва, стандартное время)",
    letterMessage: "текст про договор на поставку 100 тонн леса",
    letterSender: {
      name: "ООО 'деСталь'",
      email: "sever@destal.com"
    },
    letterRecipients: [
    {
      name: "ООО 'МТС'",
      email: "mts@mts.com"
    }]
  },

  {
    type: "out",
    specialType: false,
    status: "readed",
    withFile: false,
    historyInfo: {
      containHis: true,
      countHistory: 9,
    },
    letterTitle: 'Договор на поставку 3 тонн лесcа',
    date: "Wed Jun 12 2019 12:40:48 GMT+0300 (Москва, стандартное время)",
    letterMessage: "текст про договор на поставку 3 тонн лесcа",
    letterSender: {
      name: "ООО 'деСталь'",
      email: "sever@destal.com"
    },
    letterRecipients: [
    {
      name: "ООО 'МТС'",
      email: "mts@mts.com"
    }]
  },

  {
    type: "in",
    specialType: false,
    status: "new",
    withFile: false,
    historyInfo: {
      containHis: false,
      countHistory: 0,
    },
    letterTitle: 'Так писем не ждут',
    date: "Tue Jun 04 2019 22:12:42 GMT+0300 (Москва, стандартное время)",
    letterMessage: "Договор на поставку 100 тонн лесcа",
    letterSender: {
      name: "ООО 'деСталь'",
      email: "sever@destal.com"
    },
    letterRecipients: [
      {
        name: "ООО 'МТС'",
        email: "mts@mts.com"
      }
    ]
  },

  {
    type: "out",
    specialType: false,
    status: "readed",
    withFile: false,
    historyInfo: {
      containHis: true,
      countHistory: 9,
    },
    letterTitle: 'Так ждут письма',
    date: "Wed Jun 12 2019 12:40:48 GMT+0300 (Москва, стандартное время)",
    letterMessage: "Высылаю Вам на ознакомление исправленное доп.соглашение к текущему договору.",
    letterSender: {
      name: "ООО 'деСталь'",
      email: "sever@destal.com"
    },
    letterRecipients: [
      {
        name: "ООО 'МТС'",
        email: "mts@mts.com"
      },
      {
        name: "ЗАО 'СТМ'",
        email: "stm@mts.com"
      },
      {
        name: "ООО 'НТС НПЗ НИОКР'",
        email: "sts@ss.com"
      },
    ]
  },
]