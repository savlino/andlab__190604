import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
  html,
  body {
    height: 100%;
    width: 100%;
  }

  body {
    font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  body.fontLoaded {
    font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  #app {
    background-color: #fafafa;
    min-height: 100%;
    min-width: 100%;
  }

  p,
  label {
    font-family: Georgia, Times, 'Times New Roman', serif;
    line-height: 1.5em;
  }

  .header-body {
    position: fixed;
    left: 0;
    right: 250px;
    min-height: 140px;
    background-color: white;
    padding: 25px;
  }

  .clearfix::after {
    content: "";
    display: table;
    clear: both;
  }

  .header-container-left {
    min-width: 120px;
    padding-left: 15px;
    float: left;
    display: flex;
    flex-flow: column wrap;
  }

  .header_logo {
    outline: 0;    
  }

  .button-back {
    background-color: #fff;
    margin: 20px auto 5px;
    cursor: pointer;
  }

  .button-back:before {    
    content: '◂';
    position: relative;
    width: 10px;
    height: 10px;
    left: -8px;
    display: inline-block;
    padding: 5px;
    pointer-events: none;
  }

  .header-container-right {
    min-width: 410px;
    text-align: baseline;
    float: right;
    display: flex;
    flex-flow: row wrap;
  }

  .account-selector {
    width: 240px;
    padding: 5px;
    border-radius: 15px;
    background-color: #fff;
    margin: 10px;
    cursor: pointer;
    appearance: none;
    outline: 0;
    border: 0 !important;
  }

  .select-container:after {
    content: '▼';
    position: relative;
    right: 42px;
    display: inline-block;
    padding: 5px;
    pointer-events: none;
  }

  .header-button {
    outline: 0;
    background-color: #fff;
    width: 26px;
    height: 26px;
    padding: 0;
    margin: 10px;
    cursor: pointer;
  }

  .unreads-bubble {
  	width: auto;
  	height: 21px;
    position: relative;
    display: inline-block;
    right: 20px;
    padding: 2px 8px;
    border-radius: 10px;
    text-align: center;
    background-color: red;
    color: #fff;
    font-size: 15px;
  }

  .sidebar-container {
    width: 250px;
    height: 100%;
    position: fixed;
    top: 0px;
    right: 0px;
    background-color: blue;
    z-index: 1;
    color: rgba(255,255,255,0.6);
    font-size: 14px;
  }

  .accountinfo-container {
    height: 15%;
    padding: 0 10px;
    display: flex;
    flex-flow: row nowrap;
    align-items: center;
  }

  .avatar-container {
    margin: 5px;
  }

  .account-name {
    margin: 10px;
    vertical-align: text-top;
  }

  .menu-container {
    height: 80%;
    margin: 0;
    padding: 10px 0;
    display: flex;
    flex-flow: column wrap;
    justify-content: space-between;
  }

  menu {
    list-style-type: none;
  }

  li {
    padding: 5px 8px;
    cursor: pointer;
  }
  
  .selected-item,
  li:hover {
    color: #fff;
  }

  .menu-bubble {
    width: auto;
    height: 15px;
    padding: 1px 5px;
    color: blue;
    background-color: #fff;
    border-radius: 10px;
    font-size: 12px;
    text-align: center;
    float: right;
    // vertical-align: baseline;
    position: relative;
    right: 110px;
  }

  .submenu {
    padding: 5px 0;
  }

  .submenu:before {
    content: '▸';
    position: relative;
    left: -22px;
    display: inline-block;
    padding: 0;
    margin: 0;
    pointer-events: none;
  }

  .menu-button {
    margin: 35px;
    outline: 0;
  }

  .chat-link {
    position: relative;
    left: 170px;
    bottom: 22px;
    display: block;
    width: 58px;
    height: 40px;
    padding: 8px 15px;
    background-color: #00aaff;
    border-radius: 23px;
    outline: 0;
  }

  .chat-link:hover {
    background-color: grey;
  }

  .head-container {
    position: fixed;
    left: 0;
    right: 250px;
    top: 140px;
    min-height: 40px;
    background-color: white;
    padding: 0 53px 5px 60px;
  }

  .head-name {
    float: left;
    margin: 0;
    vertical-align: baseline;
  }

  .head-button {
    float: right;
    padding: 12px 15px;
    margin: 0 10px;
    font-size: 12px;
    outline: 0;
  }

  .white-one {
    background-color: #fff;
    color: blue
    border: solid 1px blue;
    border-radius: 5px;
  }

  .white-one:hover {
    color: white;
    background-color: blue;
  }

  .blue-one {
    color: white;
    background-color: blue;
    border: solid 1px blue;
    border-radius: 5px;
  }

  .blue-one:hover {
    color: blue;
    background-color: #fff;
    border: solid 1px blue;
    border-radius: 5px;
  }

  .modal {
  	height: 550px;
    position: absolute;
    top: 60px;
    left: 150px;
    right: 380px;
    bottom: 40px;
    padding: 20px;
    box-shadow: 0 0 50px rgba(0,0,0,0.5);
    background: #fff;
    overflow: auto;
    WebkitOverflowScrolling: touch;
    outline: none;
  }

  .show-message-modal {
  	padding: 20px 40px;
  }

  .modal-close {
  	font-size: 30px;
  	position: relative;
  	top: 7px;
  	left: 93%;
    outline: 0;
    border: none;
    background-color: #fff;
    cursor: pointer;
  }

  .new-message-form {
    color: #909090;
  }

  .new-message-field {
  	width: 100%;
  	height: 40px;
  	margin: 10px 0 5px;
  	border: none;
  	border-bottom: solid 1px #909090;
  	outline: 0;
   	appearance: none;
    font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  .new-msg-dropdown {
  	margin: 0;
  	padding: 0;
  }

  .error-form {
  	border-bottom: solid 1px red;
  }

  .error-textarea {
  	border: solid 1px red;
  }

  .new-message-dropdown {
  	position: absolute;
  	width: 85%;
  	background-color: white;
  }

  .sender-select {  	
  	width: 100%;
  }

  .sender-select:after {
    z-index: 99;
    content: '▼';
    color: black;
    position: absolute;
    right: 25px;
    display: inline-block;
    padding: 5px;
    pointer-events: none;
  }

  .contact-select {
  	width: 98%;
  	display: flex;
  }

  .contact-select:after {
    z-index: 99;
    content: '▼';
    color: black;
    position: absolute;
    right: 90px;
    display: inline-block;
    padding: 5px;
    pointer-events: none;
  }

  .adress-line {
  	color: #909090;
  	font-size: 12px;
  }

  .def_ava {
  	float: right;
  	position: relative;
  	top: -5px;
  	left: 10px;
  	float: right;
  	padding: 6px;
  	border: solid 1px #909090;
  	border-radius: 3px;
  }

  .new-message-text {
  	width: 100%;
  	padding: 15px;
  }

  .attached-file {
  	width: 100%;
  	display: flex;
  	margin: 20px 0;
  	font-size: 12px;
  	cursor: pointer;
  	outline: 0;
  	text-decoration: none;
  }

  .file-type {
  	color: blue;
  	font-weight: bold;
  }

  .file-name {
  	color: blue;
  	margin-left: 15px;
  	margin-right: 30px;
  	text-decoration: none;
  }

  .file-size {
  	color: #909090;
  }

  .file-detach {
  	margin-left: 15px;
  	cursor: pointer;
  }

  .file-attach {
  	display: flex;
  	align-items: center;
  	font-size: 12px;
  	cursor: pointer;
  }

  .file-attach-name {
  	color: blue;
  	border: none;
  	margin-left: 15px;
  	pointer-events: none;
  }

  .new-msg-btn {
    padding: 15px 40px;
    margin: 30px 0;
    font-size: 12px;
    outline: 0;
    cursor: pointer; 	
  }

  .modal-btn-prime {
    float: left;
    color: white;
    background-color: blue;
    border: solid 1px blue;
    border-radius: 5px;
  }

  .modal-btn-prime:hover {
    color: blue;
    background-color: #fff;
  }

  .modal-btn-second {
  	margin-left: 15px;
    color: blue;
    border: solid 1px white;
  }

  .modal-btn-second:hover {
    border: solid 1px blue;
    border-radius: 5px;
  }

  .error {
  	color: red;
    font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
    font-size: 12px;
  }

  .letters-nav-container {
    position: fixed;
    left: 0;
    right: 250px;
    top: 185px;
    min-height: 40px;
    padding: 0 53px 5px 60px;
    background-color: white;
  }

  .letters-nav-btn {
    margin-right: 35px;
    padding: 10px 0;
    border: none;
    outline: 0;
    background: none;
    font-size: 14px;
    color: #909090;
    cursor: pointer;
  }

  .letters-nav-btn:hover {
    border-bottom: solid 4px blue;
    color: black;
  }

  .letters-nav-current {
    border-bottom: solid 4px blue;
    color: black;    
  }

  .search-container {
    position: fixed;
    left: 0;
    right: 250px;
    top: 246px;
    min-height: 60px;
    background-color: white;
    padding: 0 0 5px 60px;
  }

  .search-form {
    padding: 16px;
    outline: 0;
    z-index: 1;
  }

  .text-form {
  	position: relative;
  	left: 0px;
    width: 64%;
  }

  .date-form {
  	float: left;
  	padding: 0;
  	margin: 0;
  }

  .search-date-input {
    float: left;
  	color: #909090;
    font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
  	height: 60px;
  	width: 245px;
    outline: 0;
  }

  .dropdown {
  	position: absolute;
    top: 60px;
  	width: 25%;
  	background-color: white;
  }

  .dropdown-elem {
  	padding: 10px 5px;
    font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
    color: black;
    cursor: pointer;
  }

  .dropdown-elem:hover {
  	background-color: #ccc;
  }

  .with-arrow:after {
    z-index: 99;
    content: '▼';
    color: black;
    position: absolute;
    left: 275px;
    top: 15px;
    display: inline-block;
    padding: 5px;
    pointer-events: none;
  }

  .letters-container {
    width: 80%;
    position: fixed;
    left: 0;
    right: 250px;
    top: 311px;
    background-color: white;
  }

  .all-check {
    display: block;
    padding: 10px 60px;
    background-color: white;
    color: #909090;
    outline: 0;
  }

  .letter-items-container {
    background-color: white;
    width: 100%;
    padding-left: 60px;
    overflow: auto;
    WebkitOverflowScrolling: touch;
  }

  .letter-item {
    display: float;
    height: 90px;
    font-size: 14px;
    color: #909090;
    line-height: 1.5;
    cursor: pointer;
    text-decoration: none;
    outline: 0;
  }

  letter-item-clickarea {
    display: float;
    height: 90px;
    font-size: 14px;
    color: #909090;
    line-height: 1.5;
    cursor: pointer;
    text-decoration: none;
    outline: 0;  	
  }

  .letter-checkbox {
    margin: auto 0;
    outline: 0;
  }

  .letter-left {
    width: 25%;
    position: relative;
    left: 0;
    top: 0;
    display: flex;
    align-items: center;
  }

  .letter-date {
    padding: 0 25px;
  }

  .letter-right {
    width: 55%;
    padding: 20px;
    vertical-align: middle;
  }

  .item-title {
    color: black;
    font-size: 16px;
    font-weight: bold;
  }

  .item-text {
	  width: 230px;
	  white-space: nowrap;
	  overflow: hidden;
	  text-overflow: ellipsis;
  }

  .new-item-sign {
  	float: left;
  	position: relative;
  	top: 6px;
  	margin-right: 5px;
  	width: 10px;
  	height: 10px;
  	border-radius: 5px;
  	background-color: blue;
  }

  .imp-item-sign {
  	float: left;
  	position: relative;
  	top: 2px;
  	padding: 2px 7px;
  	margin-right: 5px;
  	border-radius: 15px;
  	vertical-align: middle;
  	color: white;
  	font-size: 10px;
  	background-color: blue;	
  }

  .letter-extras {
    width: 15%;
    padding: 0 50px 0 10px;
    display: flex;
    justify-content: space-between;
    align-items: center;
  }

  .letter-history {
    display: flex;
    align-items: center;  	
  }

  .letter-credits {
  	line-height: 1.5;
		font-size: 14px;
  }

  .letter-credits-title {
  	float: left;
  	color: #909090;
  }

  .letter-credits-value {
  	padding-left: 15px;
  	display: flex;
  }

	.letter-credits-mail {
		float: right;
		padding: 4px 0 0 7px;
		color: #909090;
		font-size: 12px;
	}

	.letter-credits-moment {
		float: right;
		color: #909090;
	}

	.extra-recipients-btn {
		color: blue;
		border: none;
		cursor: pointer;
		outline: 0;
	}

	.extra-recipients-btn:after {
    content: '▼';
    position: relative;
    left: 2px;
    pointer-events: none;		
	}

	.recip-dropdown {
		width: 50%;
		position: absolute;
		background-color: white;
	}

	.recip-dropdown-elem {
		padding: 3px 8px;
		cursor: pointer;
	}

	.recip-dropdown-elem:hover {
		background-color: #ccc;
	}

	.recip-email {
		color: #909090;
		font-size: 12px;
	}

	.letter-text {
		margin-top: 30px;
		min-height: 200px;
	}

	.modal-btn-print {
		float: right;
		margin-top: 40px;
		border: none;
		outline: 0;
		cursor: pointer;
	}
`;

export default GlobalStyle;